// nodemcu 32s board

#include <dummy.h>

#include "ThingSpeak.h"

unsigned long myChannelNumber = 694024;
const char * myWriteAPIKey = "ILF8CUBZ6UR8RMCF";

#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 4

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

// arrays to hold device address
DeviceAddress insideThermometer;


#include <WiFi.h>

char ssid[] = "IGNACE";   // your network SSID (name)
char pass[] = "123piano";   // your network password
int keyIndex = 0;            // your network key index number (needed only for WEP)
WiFiClient  client;



void setup() {
  pinMode(2, OUTPUT);
  sensors.begin();
  Serial.begin(115200);
  delay(100);

  WiFi.mode(WIFI_STA);

  ThingSpeak.begin(client);

   if (!sensors.getAddress(insideThermometer, 0)) Serial.println("Unable to find address for Device 0"); 
  // show the addresses we found on the bus
  

  // set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
  sensors.setResolution(insideThermometer, 9);
 
  Serial.print("Device 0 Resolution: ");
  Serial.print(sensors.getResolution(insideThermometer), DEC); 
  Serial.println();
   
}

 


void loop() {
  

  // Connect or reconnect to WiFi
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    while (WiFi.status() != WL_CONNECTED) {
      digitalWrite(2, HIGH);
      WiFi.begin(ssid, pass); // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);
      digitalWrite(2, LOW);
    }
    Serial.println("\nConnected.");
  }
    
    // Measure Signal Strength (RSSI) of Wi-Fi connection
    long rssi = WiFi.RSSI();
    
    Serial.print("rssi = ");
    Serial.print(rssi);

    sensors.requestTemperatures(); // Send the command to get temperatures

    float tempC = sensors.getTempC(insideThermometer);
  if(tempC == DEVICE_DISCONNECTED_C) 
  {
    Serial.println("Error: Could not read temperature data");
    return;
  }
  Serial.print("Temp C: ");
  Serial.print(tempC);


  // Write value to Field 1 of a ThingSpeak Channel
 // int httpCode = ThingSpeak.writeField(myChannelNumber, 1, rssi, myWriteAPIKey);
//  httpCode = ThingSpeak.writeField(myChannelNumber, 2, tempC, myWriteAPIKey);

// Update the 2 ThingSpeak fields with the new data
ThingSpeak.setField(1, rssi);
ThingSpeak.setField(2, tempC);

// Write the fields that you've set all at once.
int httpCode = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  
  if (httpCode == 200) {
    Serial.println("Channel write successful.");
  }
  else {
    Serial.println("Problem writing to channel. HTTP error code " + String(httpCode));
  }

  // Wait 60 seconds to uodate the channel again
  delay(60000);
}
